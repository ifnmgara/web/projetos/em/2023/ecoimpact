from flask import Flask, render_template
import sqlite3

# Conectar ao banco de dados (ou criar se não existir)
conn = sqlite3.connect('user.db')

# Criar um cursor
cursor = conn.cursor()

# Consultar dados
cursor.execute('SELECT * FROM Componentes')
usuario1=""
componentes = cursor.fetchall()
for row in componentes:
    usuario1 = row
    print(row)    

# Fechar a conexão

app = Flask(__name__)
@app.route("/componentes")
def lista_usuarios():
    global usuario1
    return render_template("componentes.html", componentes=componentes)

app.run(debug=True)
conn.close()